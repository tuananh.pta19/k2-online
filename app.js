var express = require("express");
var path = require("path")

var app = express();
//config
var bodyPaser = require("body-parser");
app.use(bodyPaser.urlencoded({extended : false}));
app.use(bodyPaser.json())

app.use(express.static(path.join(__dirname, "public")));
//router


app.listen(process.env.PORT, function(){
    console.log("Đang kết nối server tại cổng: "+process.env.PORT);
})
